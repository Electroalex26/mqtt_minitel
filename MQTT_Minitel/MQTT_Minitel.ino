#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h> //ArduinoJSON6
DynamicJsonDocument Input(2048);
#include "config.h"

#define echolocal

WiFiClient espClient;
PubSubClient client(espClient);
String msg = "";
unsigned long old_millis = 0;
char seq_delete[] = {27,91,68,32,27,91,68};

// En cas de message MQTT
void callback(String topic, byte* message, unsigned int length) {
  String messageTemp;
  for (int i = 0; i < length; i++) {
    messageTemp += (char)message[i];
  }
  if (messageTemp.equals("Dns5ya2jvviDXxJYk4CY8wrv5RM394wMYb5h")) {
    clean_screen();
  } else {
    Serial.print(messageTemp);
  }
}

void clean_screen() {
  for (int i = 0; i < 40; i++) {
    Serial.write(10);
  }
  Serial.write(13);
  for (int i = 0; i < 40; i++) {
    Serial.write(27);
    Serial.write(91);
    Serial.write(65);
  }
}

void setup_wifi() {
  delay(10);
  WiFi.mode(WIFI_STA);
  WiFi.hostname(HostName);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
}

void reconnect() {
  while (!client.connected()) {
    //Serial.print("Attempting MQTT connection...");
    if (client.connect(HostName, mqttUser, mqttPassword)) {
      //Serial.println("connected");
      client.subscribe(topic_down);
      digitalWrite(LED_BUILTIN, HIGH);
    } else {
      digitalWrite(LED_BUILTIN, LOW);
      //Serial.print("failed, rc=");
      //Serial.print(client.state());
      //Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

void setup() {
  Serial.begin(4800, SERIAL_7E1);
  setup_wifi();
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  clean_screen();
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  // En cas de saisie clavier
  if (Serial.available() > 0) {
    char c = Serial.read();
    if (c == '\r') {
      client.publish(topic_up, msg.c_str());
      msg = "";
    } else if (c == 127) {
      msg = msg.substring(0, msg.length() - 1);
#ifdef echolocal
      for (int i = 0; i < sizeof(seq_delete); i++) {
        Serial.write(seq_delete[i]);
      }
#endif
    } else {
      msg += c;
      delay(10);
#ifdef echolocal
      Serial.write(c);
#endif
    }
  }
  // Tiens le Minitel éveillé
  if (millis() - old_millis > 20000) {
    Serial.write(27);
    old_millis = millis();
  }
  client.loop();
}
