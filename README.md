# MQTT_Minitel

Connection of a Minitel to an MQTT broker.

More details in the article : [link](https://www.f4iil.fr/hacks/connecter-un-minitel-en-mqtt/)

## Dependencies

- PubSubClient
- Arduino JSON (>= vers. 6)
